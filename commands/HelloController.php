<?php
declare(strict_types = 1);

namespace app\commands;

use ns2\common\commands\AbstractCommand;

class HelloController extends AbstractCommand
{
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
}