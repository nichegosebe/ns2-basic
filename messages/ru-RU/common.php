<?php

return [
    'Bad request' => 'Неверные параметры запроса',
    'Access denied' => 'Доступ запрещен',
    'Access denied. {permName} not granted' => 'Доступ запрещен. Право {permName} не доступно',
    'Invalid token' => 'Неверный токен',
    'Field {fieldName} is not valid UUID' => 'Поле \'{fieldName}\' не соответствует формату UUID',
    'Value {value} is not valid UUID' => '{value} не соответствует формату UUID',
];