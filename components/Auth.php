<?php
declare(strict_types = 1);

namespace app\components;

use app\modules\v1\services\traits\PermsServiceGetter;
use ns2\common\components\Auth as BasicAuth;

class Auth extends BasicAuth
{
    use PermsServiceGetter;

    /**
     * Проверка наличия права в указанных полях
     * @param string $permName Имя права
     * @param array $roles Массив имен ролей
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function checkPerm(string $permName, array $roles): bool
    {
        $result = false;
        $permService = $this->getPermsService();
        foreach ($roles as $role) {
            $result = $permService->hasPerm($role, $permName);
            if ($result) {
                break;
            }
        }

        return $result;
    }
}