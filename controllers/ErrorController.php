<?php
declare(strict_types = 1);

namespace app\controllers;

use ns2\common\controllers\ErrorController as BasicErrorController;

/**
 * Контроллер обработки ошибок
 * @package app\controllers
 */
class ErrorController extends BasicErrorController
{
}