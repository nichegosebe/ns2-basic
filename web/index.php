<?php
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

$configDir = __DIR__ . DS . '..' . DS . 'config' . DS;

require($configDir . DS . 'constants.php');

defined('YII_ENV') or define('YII_ENV', 'example');
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENABLE_EXCEPTION_HANDLER') or define('YII_ENABLE_EXCEPTION_HANDLER', true);
defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require($configDir . 'web.php');

(new yii\web\Application($config))->run();