<?php
declare(strict_types=1);

namespace app\modules\v1;

use Yii;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        Yii::$app->setContainer(require(__DIR__ . '/config/container.php'));
    }
}