<?php
declare(strict_types=1);

namespace app\modules\v1\services\traits;

use app\modules\v1\services\PermsService;

trait PermsServiceGetter
{
    /**
     * @return PermsService|object
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getPermsService(): PermsService
    {
        return \Yii::$container->get(PermsService::class);
    }
}