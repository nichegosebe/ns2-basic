<?php
declare(strict_types = 1);

namespace app\modules\v1\services;

use ns2\common\services\AbstractService;
use \Yii;

/**
 * Сервис для работы с правами
 * @package app\services
 */
class PermsService extends AbstractService
{
    // Путь к хранилищу всех возможных прав
    const PERMS_STORAGE = '@app/permissions/perms.php';

    // Список всех возможных прав
    private static $_availablePerms = null;

    /**
     * Возвращает список всех используемых в модуле прав
     * @return array
     */
    public function getAvailableList(): array
    {
        return $this->loadAvailablePerms();
    }

    /**
     * Возвращает соответствие ролей и прав сервиса
     * @return array
     */
    public function getList(): array
    {
        // TODO: implement
        return [];
    }

    /**
     * Задает ролям права
     * @param array $perms Массив, где ключ - название роли, значение - массив прав
     * @return bool
     */
    public function createMultiPerms(array $perms): bool
    {
        // TODO: implement
        return false;
    }

    /**
     * Добавляет ролям права
     * @param array $perms Массив, где ключ - название роли, значение - массив прав
     * @return bool
     */
    public function updateMultiPerms(array $perms): bool
    {
        // TODO: implement
        return false;
    }

    /**
     * Получает права указанной роли
     * @param string $roleName Имя роли
     * @return mixed|null
     */
    public function read(string $roleName)
    {
        // TODO: implement
        return [];
    }

    /**
     * Удаляет указанное право у выбранной роли
     * @param string $roleName Имя роли
     * @param string $permName Имя права
     * @return bool
     */
    public function delete(string $roleName, string $permName): bool
    {
        // TODO: implement
        return false;
    }

    /**
     * Проверяет наличие права у роли
     * @param string $roleName Имя роли
     * @param string $permName Имя права
     * @return bool
     */
    public function hasPerm(string $roleName, string $permName): bool
    {
        // TODO: implement
        return false;
    }

    /**
     * Возвращает список всех прав используемых в сервисе
     * @return array|null
     */
    private function loadAvailablePerms()
    {
        if (self::$_availablePerms === null) {
            $source = Yii::getAlias(self::PERMS_STORAGE);
            if (file_exists($source) && is_readable($source)) {
                $availablePerms = require_once($source);
                self::$_availablePerms = array_change_key_case($availablePerms, CASE_UPPER);
            } else {
                self::$_availablePerms = [];
            }
        }
        return self::$_availablePerms;
    }
}