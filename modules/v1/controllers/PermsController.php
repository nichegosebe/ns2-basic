<?php
declare(strict_types=1);

namespace app\modules\v1\controllers;

use app\modules\v1\services\traits\PermsServiceGetter;
use ns2\common\controllers\AbstractController;
use yii\helpers\ArrayHelper;

/**
 * Контроллер управления правами
 * @package app\controllers
 */
class PermsController extends AbstractController
{
    use PermsServiceGetter;

    /**
     * @api {get} /perms/available Получение списка прав используемых сервисом
     * @apiName get-available
     * @apiGroup perms
     * @apiDescription Возвращает список прав которые используются в модуле
     * @apiUse Request
     * @apiUse Response
     * @apiSuccess {Object} data Объект, где ключ - имя права, значение - его описание
     * @apiExample Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          'http://localhost/v1/perms/available'
     * @apiSampleRequest /perms/available
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionGetAvailable()
    {
        return $this->getPermsService()->getAvailableList();
    }

    /**
     * @api {get} /perms/available Получение списка ролей и доступных им прав
     * @apiName index
     * @apiGroup perms
     * @apiDescription Возвращает список ролей и досутпных им прав которые используются в сервисе
     * @apiUse Request
     * @apiUse Response
     * @apiSuccess {Object} data Объект, где ключ - имя роли, значение - объект с ключом (право) и значением(доступность права)
     * @apiExample Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          'http://localhost/v1/perms'
     * @apiSampleRequest /perms
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        return $this->getPermsService()->getList();
    }

    /**
     * @api {post} /perms Массовая установка прав ролям
     * @apiName create
     * @apiGroup perms
     * @apiDescription Удаляет все текущие права для всех ролей и устанавливает переданные
     * @apiUse Request
     * @apiUse Response
     * @apiExample Пример:
     *      URL: http://localhost/v1/perms
     *      body: {
     *          "ADMIN": [
     *              "GET_USERS_LIST",
     *              "CREATE_USER",
     *              "view_user"
     *          ]
     *      }
     *
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          -X POST -d '{"ADMIN": ["GET_USERS_LIST", "CREATE_USER", "view_user"]}'
     *          'http://localhost/v1/perms'
     * @apiSampleRequest off
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $perms = \Yii::$app->getRequest()->getBodyParams();
        return $this->getPermsService()->createMultiPerms($perms);
    }

    /**
     * @api {put} /perms Массовое добавление прав ролям
     * @apiName update
     * @apiGroup perms
     * @apiDescription Добавляет переданные права указанным ролям
     * @apiUse Request
     * @apiUse Response
     * @apiExample Пример:
     *      URL: http://localhost/v1/perms
     *      body: {
     *          "ADMIN": [
     *              "GET_USERS_LIST",
     *              "CREATE_USER",
     *              "view_user"
     *          ]
     *      }
     *
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          -X POST -d '{"ADMIN": ["GET_USERS_LIST", "CREATE_USER", "view_user"]}'
     *          'http://localhost/v1/perms'
     * @apiSampleRequest off
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionUpdate()
    {
        $perms = \Yii::$app->getRequest()->getBodyParams();
        return $this->getPermsService()->updateMultiPerms($perms);
    }

    /**
     * @api {get} /perms/:roleName Получение списка прав для указанной роли
     * @apiName view
     * @apiGroup perms
     * @apiDescription Возвращает список прав для указанной роли
     * @apiUse Request
     * @apiParam {String} roleName Имя роли
     * @apiUse Response
     * @apiSuccess {Object} data Объект, где ключ - имя права, значение - доступность права
     * @apiExample Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          'http://localhost/v1/perms/admin'
     * @apiSampleRequest /perms/:roleName
     *
     * @param string $roleName Имя роли
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionView(string $roleName)
    {
        return $this->getPermsService()->read($roleName);
    }

    /**
     * @api {delete} /perms/:roleName/:permName Удаление права у роли
     * @apiName delete
     * @apiGroup perms
     * @apiDescription Удаляет выбранное право для указанной роли
     * @apiUse Request
     * @apiParam {String} roleName Имя роли
     * @apiParam {String} permName Имя права
     * @apiUse Response
     * @apiExample Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json' -XDELETE 'http://localhost/v1/perms/admin/create_role'
     * @apiSampleRequest /perms/:roleName/:permName
     *
     * @param string $roleName Имя роли
     * @param string $permName Имя права
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionDelete(string $roleName, string $permName)
    {
        return $this->getPermsService()->delete($roleName, $permName);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET'],
            'create' => ['POST'],
            'view' => ['GET'],
            'update' => ['PUT'],
            'delete' => ['DELETE']
        ];
    }

    /**
     * @inheritdoc
     */
    public function permissions(): array
    {
        return ArrayHelper::merge(parent::permissions(), [
            'get-available' => self::ACCESS_FOR_ALL,
            'index' => 'get_perms_list',
            'create' => 'create_perms',
            'update' => 'update_perms',
            'view' => 'view_perms',
            'delete' => 'delete_perm'
        ]);
    }
}