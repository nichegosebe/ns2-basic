<?php
declare(strict_types=1);

use app\modules\v1\services\PermsService;

return [
    'singletons' => [
        PermsService::class,
    ]
];