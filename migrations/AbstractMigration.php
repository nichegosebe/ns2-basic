<?php

namespace app\migrations;

use ns2\common\migrations\AbstractSQLMigration;

class AbstractMigration extends AbstractSQLMigration
{
    protected function getSqlPath()
    {
        return '@app/migrations/sql/';
    }
}