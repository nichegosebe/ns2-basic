<?php

declare(strict_types = 1);

namespace app\exceptions;

use ns2\common\exceptions\AbstractException;

/**
 * Тестовое исключение
 * @package app\exceptions
 */
class ExampleException extends AbstractException
{
    public $defaultCode = 1000;
    public $statusCode = 409;

    public function getDefaultMessage(): string
    {
        return \Yii::t('basic', 'Test exception');
    }
}