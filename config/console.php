<?php

$componentsPath = realpath(__DIR__ . DS . YII_ENV . DS . 'components.php');
$components = ($componentsPath && file_exists($componentsPath)) ? require($componentsPath) : [];
$paramsPath = __DIR__ . DS . YII_ENV . DS . 'params.php';
$params = ($paramsPath && file_exists($paramsPath)) ? require($paramsPath) : [];

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => array_merge([], $components),
    'params' => $params
];