<?php

$componentsPath = realpath(__DIR__ . DS . YII_ENV . DS . 'test_components.php');
$components = ($componentsPath && file_exists($componentsPath)) ? require($componentsPath) : [];
$paramsPath = __DIR__ . DS . YII_ENV . DS . 'params.php';
$params = ($paramsPath && file_exists($paramsPath)) ? require($paramsPath) : [];

return [
    'id' => 'realty-tests',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'params' => $params,
    'modules' => [
        'v1' => [
            'class' => 'app\modules\v1\Module'
        ]
    ],
    'components' => array_merge([
        'request' => [
            'cookieValidationKey' => '<secret random string goes here>',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => ['ns2\common\handlers\ResponseHandler', 'beforeSend']
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/url_rules.php')
        ],
    ], $components)
];
