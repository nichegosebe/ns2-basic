<?php

return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'v1/perms',
        'pluralize' => false,
        'tokens' => [
            '{roleName}' => '<roleName:\\w+>',
            '{permName}' => '<permName:\\w+>',
        ],
        'extraPatterns' => [
            'OPTIONS <opts:.*?>' => 'options',
            'GET available' => 'get-available',
            'PUT' => 'update',
            'GET {roleName}' => 'view',
            'DELETE {roleName}/{permName}' => 'delete'
        ]
    ]
];