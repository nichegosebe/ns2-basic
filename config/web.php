<?php

$componentsPath = realpath(__DIR__ . DS . YII_ENV . DS . 'components.php');
$components = ($componentsPath && file_exists($componentsPath)) ? require($componentsPath) : [];

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'params' => [
    ],
    'modules' => [
        'v1' => [
            'class' => 'app\modules\v1\Module'
        ]
    ],
    'components' => array_merge([
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => ['ns2\common\handlers\ResponseHandler', 'beforeSend']
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/url_rules.php')
        ],
    ], $components)
];

return $config;
