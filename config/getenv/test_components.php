<?php

/**
 * Настройки компонентов для работы
 */
return [
    'sender' => [
        'class' => 'ns2\common\components\Sender'
    ],
    'queue' => [
        'class' => 'ns2\common\components\Queue',
        'host' => '127.0.0.1',
        'port' => 5672,
        'username' => 'guest',
        'password' => 'guest',
        'vhost' => '/',
    ],
    'cache' => [            // Настройки кеширования
        'class' => 'ns2\common\caching\MemCache',
        'useMemcached' => true,
        'persistentId' => 'messages_cache',
        'options' => [
            Memcached::OPT_PREFIX_KEY => 'messages_',
            Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT
        ],
        'servers' => [
            [
                'host' => '127.0.0.1',
                'port' => 11211
            ]
        ]
    ],
    'i18n' => [
        'translations' => [
            'common*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'common' => 'common.php'
                ]
            ],
            'basic*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'basic' => 'basic.php'
                ]
            ]
        ]
    ],
    'log' => [              // Настройки логирования
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'ns2\common\components\RSyslogTarget',
                'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                'url' => '127.0.0.1',
                'port' => 514,
                'serviceName' => 'messages',
                'logVars' => ['_GET', '_POST', '_FILES'],
            ],
        ],
    ],
    'db' => [               // Настройки БД
        'class' => 'ns2\common\components\Connection',
        'commandClass' => 'ns2\common\components\DbCommand',
        'charset' => 'utf8',
        'enableSchemaCache' => true,
        'schemaCacheDuration' => 3600,
        'schemaCache' => 'cache',
        'serverStatusCache' => null,
        'serverRetryInterval' => 1,
        'masterConfig' => [
            'username' => 'test_db_login',
            'password' => 'test_db_password'
        ],
        'slaveConfig' => [
            'username' => 'test_db_login',
            'password' => 'test_db_password'
        ],
        'masters' => [
            [
                'dsn' => 'pgsql:host=localhost;port=5432;dbname=test_basic'
            ]
        ],
        'slaves' => [
            [
                'dsn' => 'pgsql:host=localhost;port=5432;dbname=test_basic'
            ]
        ]
    ],
];