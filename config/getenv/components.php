<?php

use ns2\common\helpers\Config as Config;

/**
 * Настройки компонентов для работы
 */
return [
    'auth' => [
        'class' => 'app\components\Auth',
        'algo' => Config::getParam('AUTH_ALGO'), // 'HS256',
        'serviceEncodeKey' => Config::getParam('AUTH_SERVICE_ENCODE_KEY'), // 'testkey',
        'serviceDecodeKey' => Config::getParam('AUTH_SERVICE_DECODE_KEY'), // 'testkey',
        'userIdentityUrl' => Config::getParam('AUTH_USER_IDENTITY_URL'), // 'http://id.gp.local/v1/self',
        'serviceTokenName' => Config::getParam('AUTH_SERVICE_TOKEN_NAME'), // 'X-Service-Token',
        'userTokenName' => Config::getParam('AUTH_USER_TOKEN_NAME'), // 'X-User-Token'
    ],
    'sender' => [
        'class' => 'ns2\common\components\Sender'
    ],
    'queue' => [
        'class' => 'ns2\common\components\Queue',
        'host' => Config::getParam('RABBITMQ_HOST'),
        'port' => Config::getParam('RABBITMQ_PORT'),
        'username' => Config::getParam('RABBITMQ_USERNAME'),
        'password' => Config::getParam('RABBITMQ_PASSWORD'),
        'vhost' => Config::getParam('RABBITMQ_VHOST')
    ],
    'cache' => [            // Настройки кеширования
        'class' => 'ns2\common\caching\MemCache',
        'useMemcached' => true,
        'persistentId' => 'basic_cache',
        'options' => [
            Memcached::OPT_PREFIX_KEY => 'basic_',
            Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT
        ],
        'servers' => [
            [
                'host' => Config::getParam('MEMCACHED_HOST'),
                'port' => Config::getParam('MEMCACHED_PORT')
            ]
        ]
    ],
    'i18n' => [
        'translations' => [
            'common*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'common' => 'common.php'
                ]
            ],
            'basic*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'basic' => 'basic.php'
                ]
            ]
        ]
    ],
    'log' => [              // Настройки логирования
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'ns2\common\components\RSyslogTarget',
                'levels' => Config::getParamAsArray('RSYSLOG_LEVELS'),
                'url' => Config::getParam('RSYSLOG_HOST'),
                'port' => Config::getParam('RSYSLOG_PORT'),
                'serviceName' => Config::getParam('RSYSLOG_SERVICE_NAME'),
                'logVars' => Config::getParamAsArray('RSYSLOG_LOG_VARS'),
            ],
        ],
    ],
    'db' => [               // Настройки БД
        'class' => 'ns2\common\components\Connection',
        'commandClass' => 'ns2\common\components\DbCommand',
        'charset' => Config::getParam('PSQL_CHARSET'),
        'enableSchemaCache' => Config::getParam('PSQL_ENABLE_SCHEMA_CACHE'),
        'schemaCacheDuration' => Config::getParam('PSQL_SCHEMA_CACHE_DURATION'),
        'schemaCache' => Config::getParam('PSQL_SCHEMA_CACHE'),
        'serverRetryInterval' => Config::getParam('PSQL_SERVER_RETRY_INTERVAL'),
        'masterConfig' => [
            'username' => Config::getParam('PSQL_USERNAME'),
            'password' => Config::getParam('PSQL_PASSWORD'),
        ],
        'slaveConfig' => [
            'username' => Config::getParam('PSQL_USERNAME'),
            'password' => Config::getParam('PSQL_PASSWORD'),
        ],
        'masters' => [
            ['dsn' => 'pgsql:host='. Config::getParam('PSQL_MASTER_HOST') .
                            ';port=' . Config::getParam('PSQL_MASTER_PORT') . 
                            ';dbname=' . Config::getParam('PSQL_DBNAME') ]
        ],
        'slaves' => [
            ['dsn' => 'pgsql:host='. Config::getParam('PSQL_STANDBY_HOST') . 
                            ';port=' . Config::getParam('PSQL_STANDBY_PORT') . 
                            ';dbname=' . Config::getParam('PSQL_DBNAME') ]
        ]
    ],
];