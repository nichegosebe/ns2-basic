<?php

/**
 * Настройки компонентов для работы
 */
return [
    'auth' => [
        'class' => 'app\components\Auth',
        'algo' => 'HS256',
        'serviceEncodeKey' => 'testkey',
        'serviceDecodeKey' => 'testkey',
        'userIdentityUrl' => 'http://identity.local/v1/self',
        'serviceTokenName' => 'X-Service-Token',
        'userTokenName' => 'X-User-Token'
    ],
    'sender' => [           // Настройки компонента рассылки
        'class' => 'ns2\common\components\Sender'
    ],
    'queue' => [            // Настройки компонента очереди
        'class' => 'ns2\common\components\Queue',
        'host' => '127.0.0.1',
        'port' => 5672,
        'username' => 'guest',
        'password' => 'guest',
        'vhost' => '/',
    ],
    'cache' => [            // Настройки кеширования
        'class' => 'ns2\common\caching\MemCache',
        'useMemcached' => true,
        'persistentId' => 'basic_cache',
        'options' => [
            Memcached::OPT_PREFIX_KEY => 'basic_',
            Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT
        ],
        'servers' => [
            [
                'host' => '127.0.0.1',
                'port' => 11211
            ]
        ]
    ],
    'i18n' => [
        'translations' => [
            'common*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'common' => 'common.php'
                ]
            ],
            'basic*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'fileMap' => [
                    'basic' => 'basic.php'
                ]
            ]
        ]
    ],
    'log' => [              // Настройки логирования
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'ns2\common\components\RSyslogTarget',
                'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                'url' => '127.0.0.1',
                'port' => 514,
                'serviceName' => 'basic'
            ],
        ],
    ],
    'db' => [               // Настройки БД
        'class' => 'ns2\common\components\Connection',
        'commandClass' => 'ns2\common\components\DbCommand',
        'charset' => 'utf8',
        'enableSchemaCache' => true,
        'schemaCacheDuration' => 3600,
        'schemaCache' => 'cache',
        'serverStatusCache' => null,
        'serverRetryInterval' => 1,
        'masterConfig' => [
            'username' => 'db_login',
            'password' => 'db_password'
        ],
        'slaveConfig' => [
            'username' => 'db_login',
            'password' => 'db_password'
        ],
        'masters' => [
            [
                'dsn' => 'pgsql:host=localhost;port=5432;dbname=basic'
            ]
        ],
        'slaves' => [
            [
                'dsn' => 'pgsql:host=localhost;port=5432;dbname=basic'
            ]
        ]
    ],
];